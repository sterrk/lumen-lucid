<?php

namespace Lucid\Foundation;

use Illuminate\Support\Collection;

trait ServesFeaturesTrait
{
    use MarshalTrait;

    /**
     * Serve the given feature with the given arguments.
     *
     * @param string $feature
     * @param array  $arguments
     *
     * @return mixed
     */
    public function serve($feature, $arguments = [])
    {
        return $this->dispatch($this->marshal($feature, new Collection(), $arguments));
    }
}
