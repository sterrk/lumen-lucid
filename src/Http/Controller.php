<?php

namespace Lucid\Foundation\Http;

use Lucid\Foundation\ServesFeaturesTrait;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Base controller.
 */
class Controller extends BaseController
{
    use ServesFeaturesTrait;
}
